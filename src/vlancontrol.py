from ryu.base import app_manager
from ryu.controller import ofp_event
from ryu.controller.handler import CONFIG_DISPATCHER, MAIN_DISPATCHER, DEAD_DISPATCHER
from ryu.controller.handler import set_ev_cls
from ryu.ofproto import ofproto_v1_3
from ryu.lib.packet import packet
from ryu.lib.packet import ethernet
from ryu.app.wsgi import ControllerBase, WSGIApplication, route
from ryu.lib import dpid as dpid_lib
from webob import Response
import json

class VlancontrolRest(ControllerBase):
    def __init__(self, req, link, data, **config):
        super(VlancontrolRest, self).__init__(req, link, data, **config)
        self.controller_app = data['controller_app']
        self.vlan_manager = data['vlan_manager']

    @route('vlancontrol', '/vlancontrol/mactable/{dpid}', methods=['GET'], requirements={'dpid': dpid_lib.DPID_PATTERN})
    def get_mac_table(self, req, **kwargs):
        controller_app = self.controller_app
        dpid = dpid_lib.str_to_dpid(kwargs['dpid'])
        
        if dpid not in controller_app.mac_to_port.keys():
            return Response(status=404, body=f"Error 404 : switch with dpid = {dpid} not found")

        mac_table = controller_app.mac_to_port[dpid]
        body = json.dumps(mac_table)
        return Response(content_type='application/json; charset=UTF-8', body=body)
    
    @route('vlancontrol', '/vlancontrol/mactable', methods=['GET'])
    def get_mac_tables(self, req, **kwargs):
        controller_app = self.controller_app

        mac_table = controller_app.mac_to_port
        body = json.dumps(mac_table)
        return Response(content_type='application/json; charset=UTF-8', body=body)

    
    @route('vlancontrol', '/vlancontrol/vlantable/{dpid}', methods=['GET'], requirements={'dpid': dpid_lib.DPID_PATTERN})
    def get_vlan_table(self, req, **kwargs):
        vlan_manager = self.vlan_manager
        dpid = dpid_lib.str_to_dpid(kwargs['dpid'])
        
        if dpid not in vlan_manager.vlan_to_port.keys():
            return Response(status=404, body=f"Error 404 : switch with dpid = {dpid} not found")

        vlan_table = vlan_manager.vlan_to_port[dpid]
        body = json.dumps(vlan_table)
        return Response(content_type='application/json; charset=UTF-8', body=body)

    
    @route('vlancontrol', '/vlancontrol/vlantable', methods=['GET'])
    def get_vlan_tables(self, req, **kwargs):
        vlan_manager = self.vlan_manager
        vlan_table = vlan_manager.vlan_to_port

        body = json.dumps(vlan_table)
        return Response(content_type='application/json; charset=UTF-8', body=body)

    @route('vlancontrol', '/vlancontrol/addvlan', methods=['POST'])
    def add_vlan_to_port(self, req, **kwargs):
        vlan_manager = self.vlan_manager

        body = req.json if req.body else {}
        port = body.get('port')
        vlan = body.get('vlan')
        dpid = body.get('dpid')

        if dpid not in vlan_manager.vlan_to_port.keys():
            return Response(status=404, body=f"Error 404 : switch with dpid = {dpid} not found")

        if port is None or vlan is None:
            return Response(status=400, body="Error 400 : Missing parameters")
    
        if port in vlan_manager.vlan_to_port[dpid].keys():
            return Response(status=400, body="Error 400 : This port alreay exist in vlan table")

        vlan_manager.update_vlan_to_port(dpid, port, vlan)
        return Response(status=200)
    
    @route('vlancontrol', '/vlancontrol/setvlan', methods=['POST'])
    def set_vlan_to_port(self, req, **kwargs):
        print("Appel à la méthode setvlan")
        vlan_manager = self.vlan_manager

        body = req.json if req.body else {}
        port = body.get('port')
        vlan = body.get('vlan')
        dpid = body.get('dpid')

        if dpid not in vlan_manager.vlan_to_port.keys():
            return Response(status=404, body=f"Error 404 : switch with dpid = {dpid} not found")

        if port is None or vlan is None:
            return Response(status=400, body="Error 400 : Missing parameters")
        
        if port not in self.vlan_manager.vlan_to_port[dpid].keys():
            return Response(status=400, body="Error 400 : This port doesn't exist in the switch")

        vlan_manager.update_vlan_to_port(dpid, port, vlan)
        return Response(status=200)
    
    @route('vlancontrol', '/vlancontrol/setvlanlabel', methods=['POST'])
    def set_vlan_label(self, req, **kwargs):
        print("Appel de la méthode set_vlan_label")

        vlan_manager = self.vlan_manager

        body = req.json if req.body else {}
        vlan = body.get('vlan')
        label = body.get('label')

        if vlan is None or label is None:
            return Response(status=400, body="Error 400 : Missing parameters")

        elif vlan not in vlan_manager.vlan_label.keys():
            return Response(status=404, body=f"Error 404 : vlan id = {vlan} not found")
        
        vlan_manager.set_vlan_label(vlan, label)
        return Response(status=200)
    
    @route('vlancontrol', '/vlancontrol/addvlanlabel', methods=['POST'])
    def add_vlan_label(self, req, **kwargs):
        print("Appel de la méthode add_vlan_label")

        vlan_manager = self.vlan_manager

        body = req.json if req.body else {}
        vlan = body.get('vlan')
        label = body.get('label')

        if vlan is None or label is None:
            return Response(status=400, body="Error 400 : Missing parameters")

        elif vlan in vlan_manager.vlan_label.keys():
            return Response(status=404, body=f"Error 404 : vlan id = {vlan} alreay exist")
        
        vlan_manager.add_vlan_label(vlan, label)
        return Response(status=200)
    
    #récupération du label de chaque vlan
    @route('vlancontrol', '/vlancontrol/vlanlabel', methods=['GET'])
    def get_vlan_label(self, req, **kwargs):
        print("Appel à la méthode get_vlan_label")
        vlan_manager = self.vlan_manager
        vlan_label = vlan_manager.vlan_label

        body = json.dumps(vlan_label)
        return Response(content_type='application/json; charset=UTF-8', body=body)
    
    


# Manipulation et modification de la table vlan_to_port
class VlanManager(object):
    def __init__(self):
        #table d'administration vlan_to_port[dpid][port] = vlan
        self.vlan_to_port = {}

        #1 label pour chaque vlan vlan_label[vlanId] = label
        # par défault, i n'y a que le vlan 1
        self.vlan_label = { 1 : None}

    
    def update_vlan_to_port(self, dpid, port, vlan):
        self.vlan_to_port[dpid][port] = vlan

    def add_new_switch(self, dpid):
        if dpid not in self.vlan_to_port.keys():
            self.vlan_to_port[dpid] = {}

    def get_vlan_from_port(self, dpid, port):
        return self.vlan_to_port[dpid][port]
    
    def get_vlan_to_port(self):
        return self.vlan_to_port
    
    def add_vlan_label(self, vlan, label):
        if vlan not in self.vlan_label.keys():
            self.vlan_label[vlan] = label
    
    def set_vlan_label(self, vlan, label):
        if vlan in self.vlan_label.keys():
            self.vlan_label[vlan] = label

    def get_label_from_vlan(self, vlan):
        if vlan in self.vlan_label.keys():
            return self.vlan_label[vlan]
        else:
            return None
    
    def get_vlan_label(self):
        return self.vlan_label
    
    def print(self):
        for dpid in self.vlan_to_port.keys():
            print("switch dpid =", dpid)
            for port, vlan in self.vlan_to_port[dpid].items():
                print(port,'\t',vlan)
    


class Controller(app_manager.RyuApp):
    OFP_VERSIONS = [ofproto_v1_3.OFP_VERSION]

    _CONTEXTS = {
        'wsgi': WSGIApplication,
        'vlan_manager': VlanManager
    }

    def __init__(self, *args, **kwargs):
        super(Controller, self).__init__(*args, **kwargs)
        # mac_to_port[dpid][mac] = port
        self.mac_to_port = {}

        # link between Vlancontrol and the REST API
        wsgi = kwargs['wsgi']
        wsgi.register(VlancontrolRest, {'controller_app': self, 'vlan_manager': kwargs['vlan_manager']})
        self.vlan_manager = kwargs['vlan_manager']


    @set_ev_cls(ofp_event.EventOFPStateChange, MAIN_DISPATCHER)
    def new_switch(self, ev):
        datapath = ev.datapath
        dpid = datapath.id

        print("Connexion d'un nouveau switch dpid =", dpid)

        # Initialisation de la table Mac-Port
        if dpid in self.mac_to_port.keys():
            self.mac_to_port[dpid].clear()
        else:
            self.mac_to_port[dpid] = {}
        
        # Initialisation de la table Vlan-Port
        self.vlan_manager.add_new_switch(dpid)

    @set_ev_cls(ofp_event.EventOFPStateChange, DEAD_DISPATCHER)
    def left_switch(self, ev):
        datapath = ev.datapath
        dpid = datapath.id

        print("Déconnexion du switch dpid =", dpid)

        # Delete the dpid entry from mac_to_port if exist
        if dpid in self.mac_to_port.keys():
            del self.mac_to_port[dpid]
        

    @set_ev_cls(ofp_event.EventOFPSwitchFeatures, CONFIG_DISPATCHER)
    def switch_features_handler(self, ev):
        datapath = ev.msg.datapath
        ofproto = datapath.ofproto
        parser = datapath.ofproto_parser

        # install the table-miss flow entry to the controller
        match = parser.OFPMatch()
        actions = [parser.OFPActionOutput(ofproto.OFPP_CONTROLLER,
                                          ofproto.OFPCML_NO_BUFFER)]
        self.add_flow(datapath, 0, match, actions)

    def add_flow(self, datapath, priority, match, actions):
        ofproto = datapath.ofproto
        parser = datapath.ofproto_parser

        # construct flow_mod message and send it.
        inst = [parser.OFPInstructionActions(ofproto.OFPIT_APPLY_ACTIONS,
                                             actions)]
        mod = parser.OFPFlowMod(datapath=datapath, priority=priority,
                                match=match, instructions=inst)
        datapath.send_msg(mod)

    def del_flows(self, datapath):
        ofproto = datapath.ofproto
        parser = datapath.ofproto_parser
        match = parser.OFPMatch()
        inst = []
        mod = parser.OFPFlowMod(datapath, 0, 0, 0, ofproto.OFPFC_DELETE, 0, 0, 0, ofproto.OFPCML_NO_BUFFER, ofproto.OFPP_ANY, ofproto.OFPG_ANY, 0, match, inst)
        datapath.send_msg(mod)


    @set_ev_cls(ofp_event.EventOFPPacketIn, MAIN_DISPATCHER)
    def packet_in_handler(self, ev):
        msg = ev.msg
        datapath = msg.datapath
        ofproto = datapath.ofproto
        parser = datapath.ofproto_parser

        # get Datapath ID to identify OpenFlow switches.
        dpid = datapath.id
        self.mac_to_port.setdefault(dpid, {})

        # analyse the received packets using the packet library.
        pkt = packet.Packet(msg.data)
        eth_pkt = pkt.get_protocol(ethernet.ethernet)
        dst = eth_pkt.dst
        src = eth_pkt.src


        # get the received port number from packet_in message.
        in_port = msg.match['in_port']

        # learn a mac address to avoid FLOOD next time.
        self.mac_to_port[dpid][src] = in_port



        # Put all switch ports in vlan 1 by default
        # assure que chaque port a un vlan associé dans vlan_to_port
        if in_port not in self.vlan_manager.vlan_to_port[dpid].keys():
            print("Ajout du port", in_port, "dans la table des vlan pour le switch", dpid)
            self.vlan_manager.update_vlan_to_port(dpid, in_port, 1)
        
        in_vlan = self.vlan_manager.get_vlan_from_port(dpid, in_port)

        # if the destination mac address is already learned,
        # decide which port to output the packet, otherwise FLOOD.
        if dst in self.mac_to_port[dpid]:
            out_port = self.mac_to_port[dpid][dst]
        else:
            out_port = ofproto.OFPP_FLOOD

        # construct action list.
        actions = [parser.OFPActionOutput(out_port)]

        # install a flow to avoid packet_in next time.
        if out_port != ofproto.OFPP_FLOOD:
            out_vlan = self.vlan_manager.get_vlan_from_port(dpid, out_port)

            #si les deux vlans sont identiques alors on autorise le traffic
            match = parser.OFPMatch(in_port=in_port, eth_dst=dst)
            if in_vlan == out_vlan:
                #on autorise la trame
                print("Les deux vlans sont égaux vlan =", out_vlan)

                self.add_flow(datapath, 1, match, actions)
            else:
                # on supprime la trame
                actions = []
                self.add_flow(datapath, 1, match, actions)

        # construct packet_out message and send it.
        out = parser.OFPPacketOut(datapath=datapath,
                                  buffer_id=ofproto.OFP_NO_BUFFER,
                                  in_port=in_port, actions=actions,
                                  data=msg.data)
        datapath.send_msg(out)